const merge = require('webpack-merge');
const common = require('./webpack/common.config.js');
const production = require('./webpack/production.config.js');
const development = require('./webpack/development.config.js');
const devServer = require('./webpack/dev-server.config.js');
const css = require('./webpack/css.config.js');
const html = require('./webpack/html.config.js');
const script = require('./webpack/script.config.js');

module.exports = (env, argv) => {
  if (argv.mode === 'production') {
    return merge.smart([
      common,
      css,
      html,
      script,
      production,
    ]);
  }
  if (argv.mode === 'development') {
    return merge.smart([
      common,
      css,
      html,
      script,
      devServer,
      development,
    ]);
  }
  return {};
};
