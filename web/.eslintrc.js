module.exports = {
  root: true,
  parser: 'vue-eslint-parser',
  env: {
    browser: true,
  },
  extends: [
    'plugin:vue/essential',
    'eslint:recommended',
    'prettier/@typescript-eslint',
    'plugin:@typescript-eslint/recommended',
    'airbnb-base',
  ],
  parserOptions: {
    ecmaVersion: 2020,
    parser: '@typescript-eslint/parser',
  },
};
