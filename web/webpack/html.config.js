const HtmlWebpackPlugin = require('html-webpack-plugin');
const helpers = require('./helper.js');

module.exports = {
  module: {
    rules: [
      {
        test: /\.html$/,
        use: 'html-loader',
      },
      {
        test: /\.pug$/,
        use: 'pug-plain-loader',
      }
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      title: 'Task management application',
      favicon: './public/favicon.ico',
      template: helpers.root('public', 'index.ejs'),
      filename: './index.html',
    }),
  ],
};
