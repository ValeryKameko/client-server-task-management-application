const helpers = require('./helper.js');

module.exports = {
  entry: helpers.root('src', 'index.ts'),
  resolve: {
    extensions: ['*', '.js', '.ts', '.vue'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      '@': helpers.root('src'),
    },
  },
  output: {
    path: helpers.root('dist'),
    filename: 'src/[name].bundle.js',
    chunkFilename: 'js/[id].chunk.js',
  },
};
