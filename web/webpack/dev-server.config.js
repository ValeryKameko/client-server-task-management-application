module.exports = {
  watch: true,
  devServer: {
    port: 8080,
    hot: true,
  },
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000,
  },
};
