module.exports = {
  module: {
    rules: [
      {
        test: /.css$/,
        use: [
          'vue-style-loader',
          {
            loader: 'css-loader',
            options: {
            },
          },
          'postcss-loader',
        ],
      },
      {
        test: /\.styl(us)?$/,
        use: [
          'vue-style-loader',
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
            },
          },
          'stylus-loader',
          'postcss-loader',
        ],
      },
    ],
  },
};
