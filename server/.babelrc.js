module.exports = {
    presets: [
        [
            '@babel/env',
            {
                'useBuiltIns': 'entry',
                'corejs': 3,
            }
        ],
        '@babel/typescript',
    ],
    plugins: [
        '@babel/plugin-proposal-optional-chaining',
        '@babel/plugin-proposal-object-rest-spread',
        '@babel/plugin-proposal-class-properties',
        ['@babel/plugin-transform-runtime',
         {
             'regenerator': true,
         },
        ],
    ],
    ignore: [
        'node_modules',
        'dist',
        '**/.#*',
    ],
};
