import { Options } from 'sequelize';
import configuration from '.';

declare var module:any;

const dbConfiguration = {
    ...configuration.database.sequelize[configuration.application.environment],
    ...configuration.database.sequelize.common,
    ...configuration.database,
};

const options = <Options>{
    username: dbConfiguration.user,
    password: dbConfiguration.password,
    database: dbConfiguration.schema,
    host: dbConfiguration.host,
    dialect: 'postgres',
    logging: dbConfiguration.logging,
};
console.log(options);


export default (module).exports = {
    test: options,
    development: options,
    production: options,
};
