import dotenv from 'dotenv';
import sequelizeConfig from './dbConfiguration';

dotenv.config();

export interface IConfiguration {
    application: {
        environment: string;
        host: string;
        port: number;
        developmentLog: boolean;
    };
    database: {
        sequelize: any;
        schema: string;
        host: string;
        port: number;
        user: string;
        password: string;
    };
    logging: {
        winston: any;
    };
};

export const configuration = <IConfiguration> {
    application: {
        environment: process.env.NODE_ENV ?? 'development',
        host: process.env.APPLICATION_HOST ?? '0.0.0.0',
        port: Number.parseInt(process.env.APPLICATION_PORT || '', 10) || 9000,
        developmentLog: (process.env.DEVELOPMENT_LOG && process.env.DEVELOPMENT_LOG == 'true') ?? (process.env.NODE_ENV == 'development'),
    },
    database: {
        sequelize: sequelizeConfig,
        schema: process.env.DB_SCHEMA ?? 'task_management',
        host: process.env.DB_HOST ?? '127.0.0.1',
        port: process.env.DB_PORT ?? 5432,
        user: process.env.DB_USER ?? 'postgres',
        password: process.env.DB_PASSWORD ?? 'postgres',
    },
    logging: {
        winston: {
            console: {
                level: 'debug',
                handleExceptions: true,
                json: false,
                colorize: true,
            },
        },
    },
};

export default configuration;
