import path from 'path';

export default {
    common: {
        migrationStorage: 'json',
        dialect: 'postgres',
        migrationStoragePath: path.join(__dirname, 'sequelize_meta.json'),
        seederStorageTableName: 'sequelize_meta',
    },
    development: {
        logging: true,
    },
    production: {
        logging: false,
    },
};
