module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 2020,
    sourceModule: 'module',
  },
  extends: [
    'eslint:recommended',
  ],
  plugins: [
    '@typescript-eslint',
    'babel',
  ],
  env: {
    node: true,
    es2020: true,
  },
};
