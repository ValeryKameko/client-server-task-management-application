import { QueryInterface, DataTypes } from 'sequelize';
import { User } from '../models/User.model';

export async function up(queryInterface: QueryInterface) {
    queryInterface.createTable(User.TableName, {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
        },
        userName: {
            type: new DataTypes.STRING(255),
            allowNull: false,
        },
        fullName: {
            type: new DataTypes.STRING(255),
            allowNull: false,
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: false,
        },
    })
}

export async function down(queryInterface: QueryInterface) {
    return queryInterface.dropTable(User.TableName);
}
