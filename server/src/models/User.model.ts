import { Model, Sequelize, DataTypes, ModelCtor } from 'sequelize';

export class User extends Model {
    public static readonly ModelName: string = 'User';
    public static readonly ModelNamePlural: string = 'Users';
    public static readonly TableName: string = 'users';

    public id!: number;
    public userName!: string;
    public fullName!: string;
    public password!: string;
    public createdAt!: Date;
    public updatedAt!: Date;

    public static initialize(sequelize: Sequelize) {
        this.init({
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
            },
            userName: {
                type: DataTypes.STRING,
                unique: true,
            },
            fullName: DataTypes.STRING,
            password: DataTypes.STRING,
            createdAt: DataTypes.DATE,
            updatedAt: DataTypes.DATE,
        }, {
            tableName: this.TableName,
            name: {
                singular: this.ModelName,
                plural: this.ModelNamePlural,
            },
            sequelize,
        })
    }

    public static setAssociations(_: {
        [modelName: string]: ModelCtor<Model>
    }) {
    }
}
