import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import winston from 'winston';
import morgan from 'morgan';
import { configuration, IConfiguration } from '../config';

export class Application {
    private application: express.Application;
    private configuration: IConfiguration;

    constructor() {
        this.application = express();

        this.configuration = configuration;
        this.configure(configuration);
    }

    public start() {
        this.application.listen(this.configuration.application.port, configuration.application.host);
    }

    private configure(configuration: IConfiguration) {
        this.application.use(bodyParser.urlencoded({ extended: true }));
        this.application.use(cors());
        this.application.use(cookieParser());

        if (configuration.application.developmentLog) {
            const morganOptions = this.createLogger(configuration.logging.winston);
            this.application.use(morgan('combined', morganOptions));
        }
    }

    private createLogger(winstonConfiguration: any): morgan.Options {
        const logger = winston.createLogger({
            transports: [
                new winston.transports.Console(winstonConfiguration.console),
            ],
        });

        return <morgan.Options> {
            stream: {
                write: (message: string) => logger.info(message.trim()),
            },
        };
    }
}
