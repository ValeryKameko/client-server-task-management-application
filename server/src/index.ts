import { Application } from './application';

const application = new Application();

process.on('SIGINT', () => {
    process.exit();
});

application.start();
