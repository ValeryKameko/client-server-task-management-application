import { Router } from 'express';
import TasksController from './TasksController';

export const TasksRouter = Router();

TasksRouter.get('/', TasksController.index);

export default TasksRouter;
