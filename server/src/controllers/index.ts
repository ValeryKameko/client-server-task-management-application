import { Router } from 'express';
import TasksRouter from './tasks/TasksRouter';

const router = Router();

router.use('/', TasksRouter);

export default router;
