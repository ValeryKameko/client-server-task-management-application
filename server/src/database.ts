import { Sequelize } from 'sequelize';
import configuration from '../config';
import sequelizeConfig from '../config/sequelize.config';

export default new Sequelize((<any>sequelizeConfig)[configuration.application.environment]);
